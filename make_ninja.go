package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

const FILE_PATH = "build.ninja"

const NINJA_BASE =
`builddir = %s
ldflags = %s
flags = %s
compiler = %s
rule comp
    command = $compiler -MD -MF $out.d $flags -c -o $out $in
    depfile = $out.d
    description = $in
    deps = gcc

rule bin
    command = $compiler -o $out $in -MD -MF $builddir/$out.d $ldflags
    depfile = $out.d
    description = $out

`

const EXCLUDE_FILE = "Exclude"

func remove(slice []string, s int) []string {
    return append(slice[:s], slice[s+1:]...)
}

func main() {
	var cppFiles []string
	var objFiles []string
	var mainFiles []string
	var binFiles []string
	var excludedFiles []string

	var output string

	e := os.Remove( FILE_PATH )
	if e != nil && !os.IsNotExist( e ) {
		log.Fatal( e )
	}

	num_args:= len(os.Args)
	if num_args != 7 {
		fmt.Printf( "Expected 6 arguments, got %d\n", num_args - 1 )
		return
	}

	
	BUILD_DIR := os.Args[ 1 ]
	LD_FLAGS := os.Args[ 2 ]
	CXX_FLAGS := os.Args[ 3 ]
	COMPILER := os.Args[ 4 ]
	PROG_EXT := os.Args[ 5 ]
	OBJ_EXT := os.Args[ 6 ]


	err := filepath.Walk(".",
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if strings.HasSuffix( path, EXCLUDE_FILE ) {
				base_folder := strings.TrimSuffix( path, EXCLUDE_FILE )

				f, err := os.Open( path )
				if err != nil {
					return nil
				}

				scanner := bufio.NewScanner(f)
    			scanner.Split(bufio.ScanLines)

    			for scanner.Scan() {
    				name := strings.TrimSpace(scanner.Text())
    				if name != "" {
	        			excludedFiles = append(excludedFiles, base_folder + name)
    				} 
    			}
  
    			f.Close()
			}

			if !strings.HasSuffix( path, PROG_EXT ) {
				return nil
			}

			cpp := path
			obj := BUILD_DIR + "/" + strings.TrimSuffix( path, PROG_EXT ) + OBJ_EXT

			cppFiles = append( cppFiles, cpp )
			objFiles = append( objFiles, obj )

			if strings.HasPrefix( cpp, "main" ) {
				excluded := false
				for _, n := range excludedFiles {
					if n == cpp {
						excluded = true
						break
					}
				}

				if !excluded {
					pathList := strings.Split( cpp, "/" )
					mainFiles = append( mainFiles, obj )
					binFiles = append( binFiles, strings.TrimSuffix( pathList[ len( pathList ) - 1 ], PROG_EXT ) )
				}
			}

			return nil
		})


	for _, excluded := range excludedFiles {
		if strings.HasSuffix( excluded, "/" ) {
			for i := 0; i < len( cppFiles ); i++ {
				if strings.HasPrefix(cppFiles[i], excluded ) {
					cppFiles = remove(cppFiles, i)
					objFiles = remove(objFiles, i)
					i -= 1
				}
			}
		} else {
			for i := 0; i < len( cppFiles ); i++ {
				if excluded == cppFiles[i] {
					cppFiles = remove(cppFiles, i)
					objFiles = remove(objFiles, i)
					i -= 1
				}
			}
		}
	} 

	if err != nil {
		log.Fatal( err )
	}

	f, err := os.Create( FILE_PATH )
	if err != nil {
        log.Fatal( err )
	}

	output = fmt.Sprintf(NINJA_BASE, BUILD_DIR, LD_FLAGS, CXX_FLAGS, COMPILER)
	
	//obj rules
	for i := 0; i < len( cppFiles ); i++ {
		output += "build " + objFiles[ i ] + ": comp " + cppFiles[ i ] + "\n"
	}

	output += "\n"

	//remove main objects from objFiles
	for _, path := range mainFiles {
		for i := 0; i < len( objFiles ); i++ {
			if path == objFiles[ i ] {
				new_size := len( objFiles ) - 1
				objFiles[ i ] = objFiles[ new_size ]
				objFiles = objFiles[: new_size ]
				break
			}
		}
	}

	//bin rules
	for i := 0; i < len( mainFiles ); i++ {
		output += "build " + binFiles[ i ] + ": bin " + mainFiles[ i ]
		for _, path := range objFiles {
			output += " " + path 
		}
		output += "\ndefault " + binFiles[ i ] + "\n\n"
	}

	f.WriteString( output )
	f.Close()
}