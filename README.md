# Simple Go Ninja Builder

Simple script to generate the build.ninja file to build a c/c++ project
To build using ninja, you should probably still use `ninja`

For an example Makefile, check `ExampleMakefile`
This Makefile suggests that you use it as `make ninja && ninja`

## Run the program

#### Args

You should always run this script with **6** arguments :
- build directory (e.g : "build")
- ldflags (e.g : "-lm")
- cflags (e.g : "-Wall -g")
- compiler (e.g : "gcc")
- language extension (e.g : ".c")
- objects extension (e.g : ".o")

You can either run directly the script with :
```
go run make_ninja.go <args>
```

Or build an executable with :
```
go build make_ninja.go
```

This will generate a `make_ninja` executable you can then run :
```
./make_ninja <args>
```

## Architecture

Main files should be in a `main` directory.
**The executables will be named depending on the name of the main file**
For the rest, I don't think you should care about anything

#### Warning :

Every executable will be linked to every obj file because this script is very dumb. Maybe I'll find a way to make it better some day, but for now it's simple enough for me (you can still edit the build.ninja file tho)


# Licenced under WTFPL